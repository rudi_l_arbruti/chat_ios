//
//  ViewController.swift
//  ChatRoom
//
//  Created by Service Informatique on 13/02/2019.
//  Copyright © 2019 Service Informatique. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var loginTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func loginPressed(_ sender: Any) {
        let defaults = UserDefaults.standard
        let token = defaults.object(forKey: "token") as! String
        
        
        print("loginPressed \(loginTextField.text) + mon token : \(token)")
        
        //show activity indicator
        //send request
        let url = URL(string: "localhost:3000?username=\(loginTextField.text)&token=\(token)")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
        }
        
        task.resume()
        //push to next view
        // comm
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChatRoomPage")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    

}

