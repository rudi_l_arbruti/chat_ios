import UIKit

class ChatRoomViewController: UIViewController {
    
    @IBOutlet weak var newNotifLabel: UILabel!
    
    
    override func viewDidLoad() {
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onReceivedMessage(_:)), name: Notification.Name("onMessage"), object: nil)
        loadMessages()
    }
    
    @objc func onReceivedMessage(_ notification: Notification) {
        let data = notification.userInfo as! [String:String]
        newNotifLabel.text = data["message"]
        loadMessages()
       
        
    }
    
    func loadMessages(){
        let url = URL(string: "http://192.168.43.135:3000/messages")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
        }
        
        task.resume()
    }
    
    
}
